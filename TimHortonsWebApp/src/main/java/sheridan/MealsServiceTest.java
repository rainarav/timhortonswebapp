package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetAvailableMealTypes() {
		List<String> typeOfMeal = (List<String>)MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("Value", typeOfMeal !=null);
	}
	@Test
	public void testGetAvailableMealTypesException() {
		List<String> typeOfMeal = (List<String>)MealsService.getAvailableMealTypes(null);
		assertTrue("InValid", typeOfMeal.get(0).equals("No Brand Available")); 
	}
	@Test
	public void testGetAvailableMealTypesBoundaryIn() {
		List<String> typeOfMeal = (List<String>)MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertFalse("Value", typeOfMeal.size() > 2);
	}
	@Test
	public void testGetAvailableMealTypesBoundaryOut() {
		List<String> typeOfMeal = (List<String>)MealsService.getAvailableMealTypes(null);
		assertTrue("Value", typeOfMeal.size() ==1);

}
}
